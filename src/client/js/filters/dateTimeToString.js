/**
 * @copyright IMS Messsysteme GmbH, Copyright (c) 2016. All rights reserved.
 * @file Angular filter for displaying dates
*/

/* global moment */

'use strict';

var dateFormat = 'YYYY-MM-DD';
var timeFormat = 'HH:mm:ss';

function DateTimeToString() {
    return function (dateTime) {
        if(!dateTime) return '';
        var localTime = moment.utc(dateTime).toDate();
        localTime = moment(localTime).format(dateFormat + ' ' + timeFormat);
        return localTime;
    };
}

angular.module('dashboard').filter('dateTimeToString', DateTimeToString);


function DateToString() {
    return function (dateTime) {
        if(!dateTime) return '';
        var localTime = moment.utc(dateTime).toDate();
        localTime = moment(localTime).format(dateFormat);
        return localTime;
    };
}

angular.module('dashboard').filter('dateToString', DateToString);

function TimeToString() {
    return function (dateTime) {
        if(!dateTime) return '';
        var localTime = moment.utc(dateTime).toDate();
        localTime = moment(localTime).format(timeFormat);
        return localTime;
    };
}

angular.module('dashboard').filter('timeToString', TimeToString);
