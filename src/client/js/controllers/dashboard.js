/**
 * @copyright IMS Messsysteme GmbH, Copyright (c) 2016. All rights reserved.
 */

'use strict';

if (!String.prototype.startsWith) {
    String.prototype.startsWith = function (searchString, position) {
        position = position || 0;
        return this.indexOf(searchString, position) === position;
    };
}

function deleteAngularProperties(obj) {
    for (var id in obj) {
        if (id.startsWith && id.startsWith('$')) {
            delete obj[id];
        }
    }
}

function DashboardController($interval, $document) {
    const vm = this;

    vm.server = {};
    vm.server.processId = undefined;
    vm.server.state = {};
    vm.server.state.isConnected = false;
    vm.server.state.tabSetClass = 'tabSet';
    vm.helpIdx = '';


    function updateData () {
    }

    $interval(function () {
        updateData();
    }, 1000);

    // first start is fast, when the calls are made here:
    updateData();
}

angular.module('dashboard').controller('DashboardController', DashboardController);
