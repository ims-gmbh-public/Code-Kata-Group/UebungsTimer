/**
 * @copyright IMS Messsysteme GmbH, Copyright (c) 2016. All rights reserved.
 */

'use strict';


angular.module('dashboard', [
    'ui.bootstrap',
    'ngMessages',
    'ngResource',
    'toggle-switch',
    'ui.grid',
    'ngTable'
]);
