/**
 * @copyright IMS Messsysteme GmbH, Copyright (c) 2016. All rights reserved.
*/
'use strict';

angular.module('dashboard').factory('ServiceCommand', function ($resource) {
    return $resource('api/restart', null, {
        'restart': {method: 'POST'}
    });
});

angular.module('dashboard').factory('TimeService', function ($resource) {
    return $resource('api/time', null, {
        "post": {method: "POST", url: "api/time/start"}
    });
});

angular.module('dashboard').factory('LoginService', function ($resource) {
    return $resource('api/login', null, {
        "post": {method: "POST", url: "api/login"}
    });
});

angular.module('dashboard').factory('FertigService', function ($resource) {
    return $resource('api/fertig', null, {
        "post": {method: "POST", url: "api/fertig"}
    });
});
