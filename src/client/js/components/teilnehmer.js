/**
 * @copyright IMS Messsysteme GmbH, Copyright (c) 2016. All rights reserved.
 */

'use strict';

angular.module('dashboard').component('teilnehmer', {
    templateUrl: 'js/components/teilnehmer.html',
    controller: function ($interval, LoginService, FertigService) {
        const vm = this;
        vm.loggedIn = false;
        vm.onLoginClicked = () =>{
            console.log("clicked: " + vm.login);
            if(angular.isDefined(vm.login) && vm.login !== "") {
                LoginService.post({name: vm.login});
                vm.loggedIn = true;
            }
        };
        vm.onFertigClicked = () =>{
            console.log("fertig: " + vm.login);
            FertigService.post({name: vm.login});
        };

    },
    bindings: {
    }
});
