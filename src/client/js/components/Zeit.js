/**
 * @copyright IMS Messsysteme GmbH, Copyright (c) 2016. All rights reserved.
 */

'use strict';

angular.module('dashboard').component('zeit', {
    templateUrl: 'js/components/Zeit.html',
    controller: function ($interval, TimeService) {
        const vm = this;
        vm.time = 0;
        vm.teilnehmer = {};

        $interval(() => {
            TimeService.get((data) => {
                vm.time = data.time;
                vm.teilnehmer = data.teilnehmer;
                console.log("time: " + vm.time);
            }, (err) => {
                console.log(err);
            });
        }, 500);

    },
    bindings: {
    }
});
