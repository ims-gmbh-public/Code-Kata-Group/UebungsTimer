/**
 * @copyright IMS Messsysteme GmbH, Copyright (c) 2016. All rights reserved.
 */

'use strict';

angular.module('dashboard').component('leiter', {
    templateUrl: 'js/components/leiter.html',
    controller: function ($interval, TimeService) {
        const vm = this;

        vm.onStartClicked = () =>{
            console.log("clicked");
            TimeService.post({seconds: 900});
        }

    },
    bindings: {
    }
});
