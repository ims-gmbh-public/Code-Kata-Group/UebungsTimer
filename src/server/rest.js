/**
 * @copyright IMS Messsysteme GmbH, Copyright (c) 2017. All rights reserved.
 */

'use strict';

const bodyParser = require('body-parser');
const favicon = require('serve-favicon');

module.exports = function (express, productName) {
    const _obj = {};
    const app = express();
    app.use(bodyParser.json({limit: 1024 * 1024/*=1MB*/, type: '*/json'}));
    app.use('/' + productName, express.Router());
    app.use('/' + productName, express.static(__dirname + '/../client/'));
    try {
        app.use(favicon(__dirname + '/../client/img/favicon.ico'));
    } catch (exc) {
    }

    _obj.app = app;

    return _obj;
};



