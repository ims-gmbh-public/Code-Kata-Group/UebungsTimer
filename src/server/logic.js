/**
 * @copyright IMS Messsysteme GmbH, Copyright (c) 2017. All rights reserved.
 */

'use strict';
const path = require('path');

module.exports = function (rest, disableTimerForTest, appAddress) {
    const _obj = {};

    let globalTimeSeconds = 900;
    let timerIsRunning = false;

    const teilnehmer = {
    };

    _obj.getTime = function (req, res) {
        res.send({time: globalTimeSeconds, teilnehmer: teilnehmer});
    };

    _obj.getLeiter = function (req, res) {
        console.log(req.ip);
        if(req.ip === "127.0.0.1") {
            res.sendFile('indexUebungsLeiter.html', { root: path.join(__dirname, '../client') });
        } else {
            res.send("NENE du nicht !");
        }
    };

    rest.app.get("/UebungsTimer/api/time", _obj.getTime);

    rest.app.get("/UebungsTimer/Leiter", _obj.getLeiter);

    rest.app.post("/UebungsTimer/api/time/start", function (req, res) {
        globalTimeSeconds = req.body.seconds;
        console.log("start: " + globalTimeSeconds);
        timerIsRunning = true;
        for(const teiln in teilnehmer) {
            teilnehmer[teiln].zeit = undefined;
        }
        res.send({msg: "OK"});
    });

    rest.app.post("/UebungsTimer/api/login", function (req, res) {
        if(req.body.name && req.body.name !== "") {
            teilnehmer[req.body.name] = {zeit: undefined};
        }
        res.send({msg: "OK"});
    });

    rest.app.post("/UebungsTimer/api/fertig", function (req, res) {
        if(req.body.name && req.body.name !== "") {
            teilnehmer[req.body.name] = {zeit: globalTimeSeconds};
        }
        res.send({msg: "OK"});
    });

    const server = rest.app.listen(1234, appAddress, function () {
    });

    if(!disableTimerForTest) {
        setInterval(countDown, 1000);
    }

    function countDown () {
        if(timerIsRunning){
            if(globalTimeSeconds === 0) {
                timerIsRunning = false;
            } else {
                globalTimeSeconds -= 1;
            }
        }
    }

    return _obj;
};



