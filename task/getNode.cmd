@REM %~dp0 is the task directory


@REM BIN is the destination directory
set BIN=%~dp0..\bin
if exist %BIN%\npm.cmd goto end


mkdir %BIN%

@REM node4 is the tag to clone:

git clone --single-branch --branch node6 https://gitlab.com/ims-gmbh-public/NodeAndNpm.git %BIN%\

rd /S /Q %BIN%\.git


:end

