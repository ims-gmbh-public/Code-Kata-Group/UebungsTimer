
@REM %~dp0 is the task directory
@SETLOCAL
call "%~dp0productname.cmd"
@SET COPYDESTINATION=\\sbnas01\share\MEVInet-G2\latest-alpha\
@REM "%~dp0..\node_modules\.bin\node" "%~dp0copyToAlphaShare.js"

copy "%~dp0..\package.json" "%COPYDESTINATION%%PRODUCTNAME%.json"
copy "%~dp0..\dist\%PRODUCTNAME%-alpha.zip" "%COPYDESTINATION%%PRODUCTNAME%.zip"
