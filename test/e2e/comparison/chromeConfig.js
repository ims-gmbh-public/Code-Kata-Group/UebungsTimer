exports.config = {
    framework: 'mocha',
    seleniumServerJar: '../../../../node_modules/protractor/selenium/selenium-server-standalone-2.52.0.jar',
    capabilities:
    {
        'browserName': 'chrome'
    },
    onPrepare: function () {
        var PixDiff = require('pix-diff');
        browser.pixdiff = new PixDiff(
            {
                basePath: 'test/e2e/comparison/screenshots',
                width: 1280,
                height: 1024
            }
        );
    }
};
