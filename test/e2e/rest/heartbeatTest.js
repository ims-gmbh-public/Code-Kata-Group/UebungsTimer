/**
 * @copyright IMS Messsysteme GmbH, Copyright (c) 2016. All rights reserved.
 * @file e2e heartbeat test file
 */

"use strict";

var childProcess = require('child_process');
var chai = require('chai');
var restClient = require('request-promise');

var packageInfos = require('../../../package.json');
var testUtils = require('../utils.js')();

chai.config.includeStack = true;
global.expect = chai.expect;

var dashboardAddress = 'http://127.0.0.1:';
var listenPort = packageInfos.imsgmbh.webPort;
var dashboardName = '/' + packageInfos.name;

const timeout = 20000;

describe("REST-Heartbeat Tests", function () {
    var serviceProcess;

    beforeEach(function (done) {
        this.timeout(timeout);
        var startOptions = { port: 0}; // set port to 0 a random port is used
        testUtils.startService(startOptions)
            .then(function (startInfos) {
                serviceProcess = startInfos.process;
                listenPort = startInfos.port;
                done();
            }, function (err) {
                done(err);
            });
    });

    afterEach(function (done) {
        this.timeout(timeout);
        testUtils.stopService(serviceProcess)
            .then(function () {
                done();
            }, function (err) {
                done(err);
            });
    });

    it("should return heartbeat with right product name and version in it", function (done) {
        var uri = dashboardAddress + listenPort + dashboardName + "/api/heartbeat";
        expect(listenPort).to.not.equal(undefined);
        expect(serviceProcess).to.not.equal(undefined);
        restClient.get({uri: uri, json: true})
            .then(function (data) {
                expect(data.productName).to.equal(packageInfos.name);
                expect(data.version).to.equal(packageInfos.version);
                done();
            }, function (err) {
                done(err);
            });
    });


});
